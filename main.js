const fs = require('fs');
const data = require('./db/db');

const region = new Set();

data.forEach( item => region.add(item.region));

const ar = Array.from(region);

fs.writeFile('./dbRegion.js', JSON.stringify(ar), (err) => console.log(err) )

