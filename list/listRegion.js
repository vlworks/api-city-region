class ListRegion {
    regionList = dbRegion;
    filterRegions = []
    itemsList;
    inputList;
    itemConstructor;

    constructor(itemsSelector, inputSelector, itemClass) {
        this.itemsList = document.querySelector(itemsSelector);
        this.inputList = document.querySelector(inputSelector);
        this.itemConstructor = new ListItem(itemClass);

        this.itemsList && this.inputList && this.init()
    }

    init(){
        this.filterRegions = new Proxy(this.filterRegions, {
            set: (target, prop, val) => {
                target[prop] = val
                this.render()
                return true
            }
        })

        this.inputList.addEventListener('input', e => this.inputHandler(e.target.value))
        this.itemsList.addEventListener('click', e => this.itemsListHandler(e.target))
    }

    inputHandler(value){
        this.filterRegions.data = this.regionList.filter( region => region.toLowerCase().match(value.trim().toLowerCase()))
    }

    itemsListHandler(target) {
        this.inputList.value = target.textContent
        this.itemsList.classList.remove('input')
    }

    render(){
        this.itemsList.innerHTML = ''

        if (this.filterRegions.data.length > 0) {
            this.itemsList.classList.add('input')

            this.filterRegions.data.forEach( item => {
                const child = this.itemConstructor.getItem(item);
                this.itemsList.appendChild(child)
            })
        }
    }
}
class ListItem {
    itemClass = '';

    constructor(itemClass) {
        this.itemClass = itemClass;
    }

    getItem(text){
        const div = document.createElement('div');
        div.classList.add(this.itemClass)
        div.textContent = text;
        return div;
    }
}

(new ListRegion('[data-list-items]', '[data-list-input]', 'list__item'))
